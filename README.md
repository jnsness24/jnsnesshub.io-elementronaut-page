# README #

Website (Twitter Bootstrap) to promote our video-game "Elementronaut"

* compiled game is inside

* Game made in Unity3D + C#

* Video 

  [Trailer](https://www.youtube.com/watch?v=Lc9dJsyS9aw)

  [Lets Play](https://www.youtube.com/watch?v=55ePymYujLY)

[Press article](https://idw-online.de/de/news639188)